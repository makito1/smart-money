import Ember from 'ember';
import DS from 'ember-data';

export default Ember.Route.extend({
    model() {
        return Ember.Object.create({
            email   : '',
            password: '',
            provider: 'password'
        });
    },
    setupController(controller, model) {
        controller.set('user', model);
        controller.set('errors', DS.Errors.create());
        controller.set('sending', false);
    },
    actions: {
        login(user) {
            var self = this;
            this.controllerFor('login').set('errors', DS.Errors.create());
            var authData = user.getProperties('email', 'password', 'provider');
            var errors   = this.controllerFor('login').get('errors');

            this.get('session').open('firebase', authData).then(() => {
                self.transitionTo('index');
            }).catch((err) => {
                errors.add('auth', err);
            }).finally(() => {
                self.controllerFor('login').set('sending', false);
            });
        }
    }
});
