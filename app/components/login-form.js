import Ember from 'ember';
import Validator from 'npm:validator';
import DS from 'ember-data';

export default Ember.Component.extend({
    validate() {
        this.set('errors', DS.Errors.create());
        this.validateUsername(this.get('user.email'));
        this.validatePassword(this.get('user.password'));
        return this.get('errors.isEmpty');
    },
    validateUsername(value) {
        var errors = this.get('errors');
        errors.remove('username');
        if (!Validator.isEmail(value)) {
            errors.add('username', 'Неверный формат email');
        }
    },
    validatePassword(value) {
        var errors = this.get('errors');
        errors.remove('password');
        if (Validator.isNull(value)) {
            errors.add('password', 'Не должно быть пустым');
        }
    },
    actions: {
        submit() {
            if (this.validate()) {
                this.set('sending', true);
                this.sendAction('action', this.get('user'));
            }
        },
        validateUsername(value) {
            this.validateUsername(value);
        },
        validatePassword(value) {
            this.validatePassword(value);
        }
    }
});
